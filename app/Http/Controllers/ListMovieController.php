<?php

namespace App\Http\Controllers;

use App\Models\ListMovie;
use Illuminate\Http\Request;

class ListMovieController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $movies = ListMovie::all();
        return response()->json($movies);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // This method is typically used to show a form for creating a new resource.
        // Since we are building an API, we may not need this method.
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'title' => 'required|string|max:255',
            'image_url' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'description' => 'required|string',
            'duration' => 'required|integer',
            'public_at' => 'required|date',
            'finished_at' => 'nullable|date',
            'language' => 'required|string|max:50',
            'genre' => 'required|string|max:50',
            'subtitle' => 'nullable|string|max:255',
            'rating' => 'nullable|string|max:10',
            'release_year' => 'nullable|integer|min:1888|max:' . date('Y'),
        ]);

        if ($request->hasFile('image_url')) {
            $imageName = time().'.'.$request->image_url->extension();  
            $request->image_url->move(public_path('images'), $imageName);
            $validated['image_url'] = $imageName;
        }

        $movie = ListMovie::create($validated);

        return response()->json([ 
            'message' => 'successfully',
            'status' => 201,
            'data' => $movie
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ListMovie  $listMovie
     * @return \Illuminate\Http\Response
     */
    public function show(ListMovie $listMovie)
    {
        return response()->json($listMovie);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ListMovie  $listMovie
     * @return \Illuminate\Http\Response
     */
    public function edit(ListMovie $listMovie)
    {
        // This method is typically used to show a form for editing a resource.
        // Since we are building an API, we may not need this method.
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ListMovie  $listMovie
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $listMovie = ListMovie::find($id);
        if(!$listMovie){
            return response()->json([
                'status' => 404,
                'messsage' => 'Movie was not found'
            ]);
        }

        $validated = $request->validate([
            'title' => 'sometimes|required|string|max:255',
            'image' => 'sometimes|required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'description' => 'sometimes|required|string',
            'duration' => 'sometimes|required|integer',
            'public_at' => 'sometimes|required|date',
            'finished_at' => 'nullable|date',
            'language' => 'sometimes|required|string|max:50',
            'genre' => 'sometimes|required|string|max:50',
            'subtitle' => 'nullable|string|max:255',
            'rating' => 'nullable|string|max:10',
            'release_year' => 'nullable|integer|min:1888|max:' . date('Y'),
        ]);

        if ($request->hasFile('image_url')) {
            if ($listMovie->image) {
                $oldImagePath = public_path($listMovie->image);
                if (file_exists($oldImagePath)) {
                    unlink($oldImagePath);
                }
            }
    
            $imageName = time().'.'.$request->image->extension();  
            $request->image->move(public_path('images'), $imageName);
            $validated['image_url'] = 'images/' . $imageName;
        }

        $listMovie->update($validated); 

        return response()->json([
            'status' => 200,
            'messsage' => 'success'
        ]);
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy(Request $request, $id)
    {
        $listMovie = ListMovie::find($id);

        if (!$listMovie) {
            return response()->json(['message' => 'Movie not found.'], 404);
        }

        $listMovie->delete();
        return response()->json(['message' => 'Movie deleted successfully.'], 200);
    }

}
