<?php

namespace App\Http\Controllers;

use App\Models\BookingModel;
use Illuminate\Http\Request;

class BookingMoiveController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $userId = $request->input('user_id');

        if (!$userId) {
            return response()->json(null);
        }

        $request->validate([
            'user_id' => 'required|exists:users,id',
        ]);

        $bookings = BookingModel::where('user_id', $userId)
            ->with(['user' => function($query) {
                $query->select('id', 'name', 'phone');
            }, 'movie'])
            ->get();


        return response()->json($bookings);
    }

    public function searchCode(Request $request)
    {
        $code = $request->input('booking_code');

        
        if (!$code) {
            return response()->json(null);
        }

        
        $request->validate([
            'booking_code' => 'required|exists:booking_movies,booking_code',
        ]);

        $bookings = BookingModel::where('booking_code', $code)
            ->with(['user' => function($query) {
                $query->select('id', 'name', 'phone');
            }, 'movie'])
            ->get();

        return response()->json($bookings);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Validate the request data
        $validatedData = $request->validate([
            'user_id' => 'required|exists:users,id',
            'movie_id' => 'required|exists:list_movies,id',
        ]);

        // Generate a unique booking code
        $bookingCode = BookingModel::generateBookingCode();

        // Create a new booking record
        $booking = BookingModel::create([
            'user_id' => $validatedData['user_id'],
            'movie_id' => $validatedData['movie_id'],
            'booking_code' => $bookingCode,
        ]);

        return response()->json($booking, 201);
    }
}
