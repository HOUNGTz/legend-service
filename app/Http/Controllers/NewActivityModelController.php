<?php

namespace App\Http\Controllers;

use App\Models\NewActivityModel;
use Illuminate\Http\Request;

class NewActivityModelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news = NewActivityModel::all();
        return response()->json([
            'status' => 200,
            'data' => $news
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'title' => 'required|string|max:255',
            'image_url' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'description' => 'required|string',
        ]);

        if ($request->hasFile('image_url')) {
            $imageName = time().'.'.$request->image_url->extension();  
            $request->image_url->move(public_path('new_activity'), $imageName);
            $validated['image_url'] = $imageName;
        }

        $news = NewActivityModel::create($validated);

        return response()->json([ 
            'status' => 201,
            'data' => $news
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\NewActivityModel  $newActivityModel
     * @return \Illuminate\Http\Response
     */
    public function show(NewActivityModel $newActivityModel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\NewActivityModel  $newActivityModel
     * @return \Illuminate\Http\Response
     */
    public function edit(NewActivityModel $newActivityModel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\NewActivityModel  $newActivityModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, NewActivityModel $newActivityModel)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\NewActivityModel  $newActivityModel
     * @return \Illuminate\Http\Response
     */
    public function destroy(NewActivityModel $newActivityModel)
    {
        //
    }
}
