<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ListMovie extends Model
{
    use HasFactory;

    protected $table = 'list_movies';

    protected $fillable = [
        'title',
        'image_url',
        'description',
        'duration',
        'public_at',
        'finished_at',
        'language',
        'genre',
        'subtitle',
        'rating',
        'release_year'
    ];

    protected $casts = [
        'public_at' => 'datetime',
        'finished_at' => 'datetime',
        'release_year' => 'integer'
    ];
}
