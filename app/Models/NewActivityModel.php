<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NewActivityModel extends Model
{
    use HasFactory;

    protected $table = 'new_activity';

    protected $fillable = [
        'title',
        'image_url',
        'description'
    ];
}
