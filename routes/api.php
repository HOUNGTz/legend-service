<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\ListMovieController;
use App\Http\Controllers\BookingMoiveController;
use App\Http\Controllers\NewActivityModelController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'middleware' => 'api',
    'prefix' => 'auth'
], function ($router) {
    Route::post('/login', [AuthController::class, 'login']);
    Route::post('/register', [AuthController::class, 'register']);
    Route::post('/logout', [AuthController::class, 'logout']);
    Route::post('/refresh', [AuthController::class, 'refresh']);
    Route::get('/user-profile', [AuthController::class, 'userProfile']);
});

Route::group([
    'middleware' => 'api',
    'prefix' => 'movie'
], function ($router) {
    Route::get('/v1/list-down', [ListMovieController::class, 'index']);
    Route::post('/v1/store-moive', [ListMovieController::class, 'store']);
    Route::post('/v1/update-moive-list/{id}', [ListMovieController::class, 'update']);
    Route::delete('/v1/delete-moive/{id}', [ListMovieController::class, 'destroy']);
});

Route::group([
    'middleware' => 'api',
    'prefix' => 'booking'
], function ($router) {
    Route::get('/v1/order-list', [BookingMoiveController::class, 'index']);
    Route::post('/v1/order', [BookingMoiveController::class, 'store']);
    Route::post('/v1/search', [BookingMoiveController::class, 'searchCode']);
});

Route::group([
    'middleware' => 'api',
    'prefix' => 'new-activity'
], function ($router) {
    Route::get('/v1/list', [NewActivityModelController::class, 'index']);
    Route::post('/v1/store', [NewActivityModelController::class, 'store']);
    Route::post('/v1/delete', [NewActivityModelController::class, 'destory']);
});