<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Api Service Endpoint</title>
</head>
<body style="background-color:beige;">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <div class="container">
        <div class="content">
            <br>
            <table class="table">
                <thead class="table table-primary">
                    <th>Name</th>
                    <th>Type</th>
                    <th>Params</th>
                    <th>Actions</th>
                </thead>
                <tbody class="table table-secondary">

                    <tr class="table table-danger"><td>Authentication</td></tr>
                    <tr>
                        <td>/api/auth/login</td>
                        <td>List</td>
                        <td>phone, password</td>
                        <td>POST</td>
                    </tr>
                    <tr>
                        <td>/api/auth/register</td>
                        <td>List</td>
                        <td>id, title, image_url, description</td>
                        <td>POST</td>
                    </tr>
                    <tr>
                        <td>/api/auth/logout</td>
                        <td>id</td>
                        <td>id</td>
                        <td>POST</td>
                    </tr>
                    <tr>
                        <td>/api/auth/refresh</td>
                        <td></td>
                        <td></td>
                        <td>POST</td>
                    </tr>
                    <tr>
                        <td>/api/auth/user-profile</td>
                        <td></td>
                        <td></td>
                        <td>GET</td>
                    </tr>

                    <tr class="table table-danger"><td>News and Activity</td></tr>
                    <tr>
                        <td>/api/new-activity/v1/list</td>
                        <td>List</td>
                        <td>id, title, image_url, description</td>
                        <td>Get</td>
                    </tr>
                    <tr>
                        <td>/api/new-activity/v1/store</td>
                        <td>List</td>
                        <td>id, title, image_url, description</td>
                        <td>POST</td>
                    </tr>
                    <tr>
                        <td>/api/new-activity/v1/delete</td>
                        <td>id</td>
                        <td>id</td>
                        <td>DELETE</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
</body>
</html>
